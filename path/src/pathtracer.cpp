#include "pathtracer.h"

#define EPSILON 1e-4
#define SKY_DIST 1000

static Random &rng = Random::common();

PathTracer::PathTracer() {}

Radiance3 PathTracer::sample(int x, int y, Rect2D viewport)
{
    Radiance3 s = Radiance3::zero();
    double dx = rng.uniform(), dy = rng.uniform();

    if (m_settings.superSamples == 1){
        Ray ray = m_world->camera()->worldRay(x + dx, y + dy, viewport);
        s=trace(ray,true);
    }else{
        int across = m_settings.superSamples;
        float currRow, currCol;
        currRow = currCol = 0.f;

        float subSize = 1.f/across;
        float originRow = x;
        float originCol = y;
        float startRow = originRow;
        float startCol = originCol;
        for (int subRow=0; subRow < across; subRow++){
            for (int subCol=0; subCol < across; subCol++){
                currRow = startRow + subSize * subRow;
                currCol = startCol + subSize * subCol;

                // Jitter in place
                double dx = rng.uniform() * subSize, dy = rng.uniform() * subSize;
                Ray ray = m_world->camera()->worldRay(x + dx, y + dy, viewport);
                s+=trace(ray,true);
            }
        }
        s /= (across * across); //Average samples
    }

    return s;
}

Radiance3 PathTracer::trace( const Ray &ray,
                      bool isEyeRay,
                      float *distance)
{
    Radiance3 final = Radiance3::zero(); // black by default
    // World::emissivePoint() expects there to be lights in the scene. Quit in that case.
    if (!m_world->lightsExist() && !m_settings.useSkyMap) return final;

    shared_ptr<Surfel> surfel;
    float dist = inf();
    m_world->intersect(ray, dist, surfel);

    if (surfel){
        // estimated Light = emitted + estimate of radiance;
        if (isEyeRay && m_settings.useEmitted){
            final += PathTracer::emittedLight(surfel, ray);
        }
        if ((!isEyeRay) || m_settings.useDirectDiffuse || m_settings.useDirectSpecular){
            final += PathTracer::estimateDirectLightAreaLights(surfel, ray);
        }
        if ((!isEyeRay) || m_settings.useIndirect){
            final += PathTracer::estimateIndirectLight(surfel, ray);
        }
    }
    return final;
}

Radiance3 PathTracer::emittedLight(const shared_ptr<Surfel> surfel, const Ray &ray){
    return (surfel->emittedRadiance(-ray.direction()));
}

Radiance3 PathTracer::estimateIndirectLight(const shared_ptr<Surfel> surfel, const Ray &ray){

    Radiance3 outIndirect = Radiance3::zero();

    float rand = rng.uniform();
    Vector3 wOut = -ray.direction();
    Vector3 wIn;
    float probabilityHint = 1.0;
    Color3 weight = Color3(1.0);
    surfel->scatter(PathDirection::EYE_TO_SOURCE, wOut, false, rng, weight, wIn, probabilityHint);

    float prob = weight.average();

    // Russian roulette termination
    if (rand < prob){
        Vector3 surfelPosOffset = surfel->position + (EPSILON * wIn);
        Ray offsetRay = Ray(surfelPosOffset, wIn);
        float* distance = nullptr;
        Radiance3 traced = PathTracer::trace(offsetRay, false, distance);
        outIndirect += weight * traced;
        outIndirect /= prob;
    }

    // Clamp indirect
    outIndirect = outIndirect.clamp(0.f, 1.f);
    return outIndirect;
}

Radiance3 PathTracer::estimateDirectLightAreaLights(const shared_ptr<Surfel> surfel, const Ray &ray){
    Radiance3 outDirect = Radiance3::zero();

    // Bail here if no lights in the scene
    if (!m_world->lightsExist() && !m_settings.useSkyMap){
        return outDirect;
    }
    // Normal light sampling
    if (m_settings.useDirectDiffuse && !m_settings.useSkyMap){
        Vector3 lumePoint;
        Tri lumeTri = Tri();
        Vector3 lumeNormal;
        float lumeProb = 0.0;
        float lumeArea = 0.0;
        m_world->emissivePoint(rng, lumePoint, lumeTri, lumeNormal, lumeProb, lumeArea);

        // If light visible from surfel point
        Vector3 surfelPosOffset = surfel->position + (EPSILON * surfel->geometricNormal);
        Vector3 lumePosOffset = lumePoint + (EPSILON * lumeNormal);
        if (m_world->lineOfSight(surfelPosOffset, lumePosOffset)){
            Vector3 wIn = lumePoint - surfel->position;
            const float distance2 = wIn.squaredLength();
            wIn /= sqrt(distance2);

            float geom = (max(0.f, wIn.dot(surfel->shadingNormal)) * max(0.f, -wIn.dot(lumeNormal)))/distance2;

            shared_ptr<UniversalMaterial> lumeMaterial = dynamic_pointer_cast<UniversalMaterial>(lumeTri.material());
            Radiance3 emissive = lumeMaterial->emissive().mean()/pif();
            Radiance3 scatter = surfel->finiteScatteringDensity(wIn.direction(), -ray.direction());
            outDirect += lumeProb * emissive * scatter * geom;
        }
    }

    if (m_settings.useDirectDiffuse && m_settings.useSkyMap){
        Vector3 skySampler = Vector3(0.0, 0.0, 0.0);
        float pdf = 0.0;
        Vector3::hemiRandom(Vector3(0.0, 1.0, 0.0), rng, skySampler, pdf);

        // If light visible from surfel point
        Vector3 surfelPosOffset = surfel->position + (EPSILON * surfel->geometricNormal);
        Vector3 skyPos = skySampler * SKY_DIST;
        if (m_world->lineOfSight(surfelPosOffset, skyPos)){
            Vector3 wIn = skyPos - surfel->position;

            float geom = max(0.f, wIn.direction().dot(surfel->shadingNormal));

            Color4 skyColor = Color4(0.f, 0.f, 0.f, 0.f);
            m_world->sampleSkybox(skySampler, skyColor);
            Radiance3 skyEmissive = Radiance3(skyColor.r, skyColor.g, skyColor.b);
            Radiance3 scatter = surfel->finiteScatteringDensity(wIn.direction(), -ray.direction());
            outDirect += skyEmissive * scatter * geom * (1/pdf);
        }
    }

    if (m_settings.useDirectSpecular){
        // Iterate through impulse-reflected light too
        Surfel::ImpulseArray impulseArray;
        surfel->getImpulses(PathDirection::EYE_TO_SOURCE, -ray.direction(), impulseArray);
        for (int i=0; i < impulseArray.size();  i++){
            const Surfel::Impulse& impulse = impulseArray[i];
            Ray ray2 = Ray(surfel->position, impulse.direction).bumpedRay(EPSILON);
            shared_ptr<Surfel> surfel2;
            float dist = inf();
            m_world->intersect(ray2, dist, surfel2);
            if (surfel2){
                outDirect += surfel2->emittedRadiance(-ray2.direction()) * impulse.magnitude;
            }
        }
    }

    return outDirect;
}

void PathTracer::setWorld(World *world)
{
    m_world=world;
}

void PathTracer::setPTSettings(PTSettings settings)
{
    m_settings=settings;
}
