
/* -*- c++ -*- */
{ 
    entities = { 
        camera = Camera { 
            depthOfFieldSettings = DepthOfFieldSettings { 
                enabled = false; 
                farBlurRadiusFraction = 0.005; 
                farBlurryPlaneZ = -100; 
                farSharpPlaneZ = -40; 
                focusPlaneZ = -10; 
                lensRadius = 0.01; 
                model = "NONE"; 
                nearBlurRadiusFraction = 0.015; 
                nearBlurryPlaneZ = -0.25; 
                nearSharpPlaneZ = -1; 
            }; 
            
            filmSettings = FilmSettings { 
                antialiasingEnabled = true; 
                antialiasingFilterRadius = 0; 
                antialiasingHighQuality = true; 
                bloomRadiusFraction = 0.015; 
                bloomStrength = 0.25; 
                debugZoom = 1; 
                effectsEnabled = true; 
                gamma = 2.2; 
                sensitivity = 1; 
                toneCurve = Spline { 
                    control = ( 0, 0.1, 0.2, 0.5, 1 ); 
                    extrapolationMode = "LINEAR"; 
                    finalInterval = -1; 
                    interpolationMode = "CUBIC"; 
                    time = ( 0, 0.1, 0.2, 0.5, 1 ); 
                }; 
                
                vignetteBottomStrength = 0.05; 
                vignetteSizeFraction = 0.17; 
                vignetteTopStrength = 0.5; 
            }; 
            
            frame = CFrame::fromXYZYPRDegrees(0, 1.5, 9, 0, 0, 0 );
            motionBlurSettings = MotionBlurSettings {
                cameraMotionInfluence = 0.5; 
                enabled = false; 
                exposureFraction = 0.75; 
                maxBlurDiameterFraction = 0.1; 
                numSamples = 27; 
            }; 
            
            projection = Projection { 
                farPlaneZ = -200; 
                fovDegrees = 20;
                fovDirection = "VERTICAL"; 
                nearPlaneZ = -1;
                pixelOffset = Vector2(0, 0 ); 
            }; 
            
            visualizationScale = 1;
        }; 
        
        cornellBox = VisibleEntity { 
            frame = CFrame::fromXYZYPRDegrees(0, 0, -0.25 ); 
            model = "cornellModel"; 
        }; 
        
        sphere = VisibleEntity { 
            frame = CFrame::fromXYZYPRDegrees(0, 0.399696, 0 ); 
            model = "sphereModel"; 
        }; 
        
        spline = SplineLight {
            frame = CFrame::fromXYZPRDegrees(0, 0, 0);
            model = "splineModel";
        };
    }; 
    
    lightingEnvironment = LightingEnvironment { 
        ambientOcclusionSettings = AmbientOcclusionSettings { 
            bias = 0.023; 
            blurRadius = 4; 
            blurStepSize = 2; 
            depthPeelSeparationHint = 0.01; 
            edgeSharpness = 1; 
            enabled = true; 
            highQualityBlur = true; 
            intensity = 1; 
            monotonicallyDecreasingBilateralWeights = false; 
            numSamples = 19; 
            packBlurKeys = false; 
            radius = 0.75; 
            temporalFilterSettings = TemporalFilter::Settings { 
                falloffEndDistance = 0.07; 
                falloffStartDistance = 0.05; 
                hysteresis = 0; 
            }; 
            
            temporallyVarySamples = false; 
            useDepthPeelBuffer = false; 
            useNormalBuffer = true; 
            useNormalsInBlur = true; 
            zStorage = "HALF"; 
        }; 
        
        environmentMap = 0.05; 
    }; 
    
    models = { 
        
        cornellModel = ArticulatedModel::Specification { 
            filename = "assets/CornellBox-Empty-CO.obj"; 
            preprocess = { 
                transformGeometry(all(), Matrix4::scale(1.5, 1.5, 1.5 ) ) }; 
            
        }; 
        
        sphereModel = ArticulatedModel::Specification { 
            filename = "assets/floatSphere.obj"; 
            preprocess = { 
                setMaterial(all(), Color3(1, 1, 1 ) ); 
                transformGeometry(all(), Matrix4::scale(0.015, 0.015, 0.015) ) }; 
            
        }; 

        splineModel = ArticulatedModel::Specification {
            filename = "model/wrap-spline.lit";
        };
        
    }; 
    
    name = "Wrap (Photon Beams)"; 
} 
